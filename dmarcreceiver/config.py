class _Config(dict):
    def read_file(self, f):
        for line in f:
            line = line.strip()
            if line.find('#') != -1:
                line = line[:line.find('#')]
            if line.find('=') != -1:
                name, value = line.split('=', 1)
                name = name.strip()
                value = value.strip()
                self[name] = value

config = _Config()

__all__ = [ 'config' ]
