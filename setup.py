# -*- coding: utf-8 -*-

import sys

try:
    from setuptools import setup, find_packages
except ImportError:
    from ez_setup import use_setuptools
    use_setuptools()
    from setuptools import setup, find_packages

install_requires=[
    'lxml',
    'sqlalchemy',
    'transaction',
    'zope.interface',
    'zope.sqlalchemy',
]

setup(
    name='DMARCReceiver',
    version='1.3',
    description='Receive DMARC reports',
    author='David Baer',
    author_email='david@amyanddavid.net',
    packages=find_packages(),
    scripts=['scripts/dmarc-receive'],
    install_requires=install_requires,
    include_package_data=True,
    zip_safe=True
)
